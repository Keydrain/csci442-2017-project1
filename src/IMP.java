/*
 *Hunter Lloyd
 * Copyrite.......I wrote, ask permission if you want to use it outside of class.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.File;

import static java.awt.Color.*;


public class IMP implements MouseListener {
    JFrame frame;
    JPanel mp;
    JButton start;
    JScrollPane scroll;
    JMenuItem openItem, exitItem, resetItem;
    Toolkit toolkit;
    File pic;
    ImageIcon img;
    int colorX, colorY;
    int[] pixels;
    int[] results;
    //Instance Fields you will be using below

    //This will be your height and width of your 2d array
    int height = 0, width = 0;
    int oheight = 0, owidth = 0;

    //your 2D array of pixels
    int picture[][];

    int originalPicture[][];

    /*
     * In the Constructor I set up the GUI, the frame the menus. The open pulldown
     * menu is how you will open an image to manipulate.
     */
    private IMP() {
        toolkit = Toolkit.getDefaultToolkit();
        frame = new JFrame("Image Processing Software by Hunter");
        JMenuBar bar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu functions = getFunctions();
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent ev) {
                quit();
            }
        });
        openItem = new JMenuItem("Open");
        openItem.addActionListener(evt -> handleOpen());
        resetItem = new JMenuItem("Reset");
        resetItem.addActionListener(evt -> reset());
        exitItem = new JMenuItem("Exit");
        exitItem.addActionListener(evt -> quit());
        file.add(openItem);
        file.add(resetItem);
        file.add(exitItem);
        bar.add(file);
        bar.add(functions);
        frame.setSize(600, 600);
        mp = new JPanel();
        mp.setBackground(new Color(0, 0, 0));
        scroll = new JScrollPane(mp);
        frame.getContentPane().add(scroll, BorderLayout.CENTER);
        JPanel butPanel = new JPanel();
        butPanel.setBackground(Color.black);
        start = new JButton("start");
        start.setEnabled(false);
        butPanel.add(start);
        frame.getContentPane().add(butPanel, BorderLayout.SOUTH);
        frame.setJMenuBar(bar);
        frame.setVisible(true);
    }

   /*
    * This method creates the pulldown menu and sets up listeners to selection of the menu choices. If the listeners are activated they call the methods
    * for handling the choice, fun1, fun2, fun3, fun4, etc. etc.
    */

    public static void main(String[] args) {
        IMP imp = new IMP();
    }

    // Added some extra things to the menus. Just a single implementation of each required functionality.
    private JMenu getFunctions() {
        JMenu fun = new JMenu("Functions");
        //JMenuItem firstItem = new JMenuItem("Example");
        JMenuItem secondItem = new JMenuItem("Rotation90");
        JMenuItem thirdItem = new JMenuItem("Histograms");
        JMenuItem fourthItem = new JMenuItem("Equalize");
        JMenuItem fifthItem = new JMenuItem("Grayscale");
        JMenuItem sixthItem = new JMenuItem("EdgeDetection");

        //firstItem.addActionListener(evt -> Example());
        secondItem.addActionListener(evt -> Rotation90());
        thirdItem.addActionListener(evt -> Histograms());
        fourthItem.addActionListener(evt -> Equalize());
        fifthItem.addActionListener(evt -> Grayscale());
        sixthItem.addActionListener(evt -> EdgeDetection());

        //fun.add(firstItem);
        fun.add(secondItem);
        fun.add(thirdItem);
        fun.add(fourthItem);
        fun.add(fifthItem);
        fun.add(sixthItem);

        return fun;

    }

    /*
     * This method handles opening an image file, breaking down the picture to a one-dimensional array and then drawing the image on the frame.
     * You don't need to worry about this method.
     */
    private void handleOpen() {
        img = new ImageIcon();
        JFileChooser chooser = new JFileChooser();
        int option = chooser.showOpenDialog(frame);
        if (option == JFileChooser.APPROVE_OPTION) {
            pic = chooser.getSelectedFile();
            img = new ImageIcon(pic.getPath());
        }
        width = img.getIconWidth();
        height = img.getIconHeight();
        owidth = width;
        oheight = height;

        JLabel label = new JLabel(img);
        label.addMouseListener(this);
        pixels = new int[width * height];

        results = new int[width * height];


        Image image = img.getImage();

        PixelGrabber pg = new PixelGrabber(image, 0, 0, width, height, pixels, 0, width);
        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
            System.err.println("Interrupted waiting for pixels");
            return;
        }
        System.arraycopy(pixels, 0, results, 0, width * height);
        turnTwoDimensional();
        mp.removeAll();
        mp.add(label);

        mp.revalidate();
    }

    /*
     * The libraries in Java give a one dimensional array of RGB values for an image, I thought a 2-Dimensional array would be more useful to you
     * So this method changes the one dimensional array to a two-dimensional.
     */
    private void turnTwoDimensional() {
        picture = new int[height][width];
        originalPicture = new int[height][width];
        for (int i = 0; i < height; i++) {
            System.arraycopy(pixels, i * width, picture[i], 0, width);
            System.arraycopy(pixels, i * width, originalPicture[i], 0, width);
        }
    }

    /*
     *  This method takes the picture back to the original picture
     */
    private void reset() {
        width = owidth;
        height = oheight;
        picture = new int[height][width];
        for (int i = 0; i < height; i++) {
            System.arraycopy(originalPicture[i], 0, picture[i], 0, width);
        }
        resetPicture();
    }

    /*
     * This method is called to redraw the screen with the new image.
     */
    private void resetPicture() {
        clear();
        for (int i = 0; i < height; i++) {
            System.arraycopy(picture[i], 0, pixels, i * width, width);
        }
        Image img2 = toolkit.createImage(new MemoryImageSource(width, height, pixels, 0, width));

        JLabel label2 = new JLabel(new ImageIcon(img2));
        label2.addMouseListener(this);
        mp.removeAll();
        mp.add(label2);

        mp.revalidate();
    }

    /*
     * This method clears the screen without removing the picture from memory.
     */
    private void clear() {
        mp.removeAll();
        mp.revalidate();
        mp.updateUI();
    }

    /*
     * This method takes a single integer value and breaks it down doing bit manipulation to 4 individual int values for A, R, G, and B values
     */
    private int[] getPixelArray(int pixel) {
        int temp[] = new int[4];
        temp[0] = (pixel >> 24) & 0xff;
        temp[1] = (pixel >> 16) & 0xff;
        temp[2] = (pixel >> 8) & 0xff;
        temp[3] = (pixel) & 0xff;
        return temp;

    }

    /*
     * This method takes an array of size 4 and combines the first 8 bits of each to create one integer.
     */
    private int getPixels(int rgb[]) {
        return (rgb[0] << 24) | (rgb[1] << 16) | (rgb[2] << 8) | rgb[3];
    }

    private void getValue() {
        int pix = picture[colorY][colorX];
        int temp[] = getPixelArray(pix);
        System.out.println("Color value " + temp[0] + " " + temp[1] + " " + temp[2] + " " + temp[3]);
    }

    /**************************************************************************************************
     * This is where you will put your methods. Every method below is called when the corresponding pulldown menu is
     * used. As long as you have a picture open first the when your fun1, fun2, fun....etc method is called you will
     * have a 2D array called picture that is holding each pixel from your picture.
     *************************************************************************************************/
   /*
    * Example function that just removes all red values from the picture.
    * Each pixel value in picture[i][j] holds an integer value. You need to send that pixel to getPixelArray the method which will return a 4 element array
    * that holds A,R,G,B values. Ignore [0], that's the Alpha channel which is transparency, we won't be using that, but you can on your own.
    * getPixelArray will breaks down your single int to 4 ints so you can manipulate the values for each level of R, G, B.
    * After you make changes and do your calculations to your pixel values the getPixels method will put the 4 values in your ARGB array back into a single
    * integer value so you can give it back to the program and display the new picture.
    */
    private void Example() {

        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++) {
                int rgbArray[] = new int[4];

                //get three ints for R, G and B
                rgbArray = getPixelArray(picture[i][j]);


                rgbArray[1] = 0;
                //take three ints for R, G, B and put them back into a single int
                picture[i][j] = getPixels(rgbArray);
            }
        resetPicture();
    }

    /*
     * fun2
     * This is where you will write your STACK
     * All the pixels are in picture[][]
     * Look at above fun1() to see how to get the RGB out of the int (getPixelArray)
     * and then put the RGB back to an int (getPixels)
     */
    private void Rotation90() {

        // Translate the image
        int newPicture[][] = new int[width][height];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                newPicture[j][i] = picture[i][j];
            }
        }

        // Reverse the rows
        picture = new int[width][height];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                picture[j][i] = newPicture[j][height - (i + 1)];
            }
        }

        // Swap the heights and width measurements
        int temp = height;
        height = width;
        width = temp;

        resetPicture();
    }

    private void Histograms() {

        // Setup frames
        JFrame redFrame = new JFrame("Red");
        redFrame.setSize(256 + 16, 512);
        redFrame.setLocation(800, 0);
        JFrame greenFrame = new JFrame("Green");
        greenFrame.setSize(256 + 16, 512);
        greenFrame.setLocation(800 + 256 + 16 + 32, 0);
        JFrame blueFrame = new JFrame("blue");
        blueFrame.setSize(256 + 16, 512);
        blueFrame.setLocation(800 + 256 + 16 + 256 + 16 + 32 + 32, 0);
        MyPanel redPanel = new MyPanel(red);
        MyPanel greenPanel = new MyPanel(green);
        MyPanel bluePanel = new MyPanel(blue);
        redFrame.getContentPane().add(redPanel, BorderLayout.CENTER);
        redFrame.setVisible(true);
        greenFrame.getContentPane().add(greenPanel, BorderLayout.CENTER);
        greenFrame.setVisible(true);
        blueFrame.getContentPane().add(bluePanel, BorderLayout.CENTER);
        blueFrame.setVisible(true);
        start.setEnabled(true);

        // When start is pushed, do these things
        start.addActionListener(evt -> {
            double rArray[] = new double[256];
            double gArray[] = new double[256];
            double bArray[] = new double[256];

            // Count each instance of the colors
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    int rgbArray[] = new int[4];

                    rgbArray = getPixelArray(picture[i][j]);

                    rArray[rgbArray[1]] += 1.0;
                    gArray[rgbArray[2]] += 1.0;
                    bArray[rgbArray[3]] += 1.0;

                }
            }

            // Discover the maximums for each color
            double rHeight = 0;
            for (double anr : rArray) {
                if (rHeight < anr) {
                    rHeight = anr;
                }
            }
            double gHeight = 0;
            for (double ang : gArray) {
                if (gHeight < ang) {
                    gHeight = ang;
                }
            }
            double bHeight = 0;
            for (double anb : bArray) {
                if (bHeight < anb) {
                    bHeight = anb;
                }
            }

            // Normalize the heights of the data points
            for (int i = 0; i < rArray.length; i++) {
                rArray[i] = rArray[i] / rHeight;
            }
            for (int i = 0; i < gArray.length; i++) {
                gArray[i] = gArray[i] / gHeight;
            }
            for (int i = 0; i < bArray.length; i++) {
                bArray[i] = bArray[i] / bHeight;
            }

            // Paint the histograms
            redPanel.loadData(rArray);
            redPanel.repaint();
            greenPanel.loadData(gArray);
            greenPanel.repaint();
            bluePanel.loadData(bArray);
            bluePanel.repaint();

        });

    }

    private void Equalize() {

        int rArray[] = new int[256];
        int gArray[] = new int[256];
        int bArray[] = new int[256];

        int Max[] = {0, 0, 0};
        int Min[] = {255, 255, 255};

        // Find min and max for each color
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgbArray[] = new int[4];

                rgbArray = getPixelArray(picture[i][j]);

                rArray[rgbArray[1]] += 1;
                gArray[rgbArray[2]] += 1;
                bArray[rgbArray[3]] += 1;

                if (rgbArray[1] > Max[0]) {
                    Max[0] = rgbArray[1];
                }
                if (rgbArray[1] < Min[0]) {
                    Min[0] = rgbArray[1];
                }
                if (rgbArray[2] > Max[1]) {
                    Max[1] = rgbArray[2];
                }
                if (rgbArray[2] < Min[1]) {
                    Min[1] = rgbArray[2];
                }
                if (rgbArray[3] > Max[2]) {
                    Max[2] = rgbArray[3];
                }
                if (rgbArray[3] < Min[2]) {
                    Min[2] = rgbArray[3];
                }

            }
        }

        // Scale all the values as placed back into the picture
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgbArray[] = new int[4];

                rgbArray = getPixelArray(picture[i][j]);

                rgbArray[1] = scaler(rgbArray[1], Min[0], Max[0], 0, 255);
                rgbArray[2] = scaler(rgbArray[2], Min[1], Max[1], 0, 255);
                rgbArray[3] = scaler(rgbArray[3], Min[2], Max[2], 0, 255);

                picture[i][j] = getPixels(rgbArray);
            }
        }

        resetPicture();

    }

    // Scales a value from one range to another range
    private int scaler(int input, int oldmin, int oldmax, int newmin, int newmax) {
        return (int) Math.round((((input - oldmin) * (newmax - newmin)) / ((oldmax - oldmin) * 1.0)) + newmin);
    }

    private void Grayscale() {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgbArray[] = new int[4];

                rgbArray = getPixelArray(picture[i][j]);

                // Remove the gamma from the three colors
                double rLinear, gLinear, bLinear;
                if (rgbArray[1] <= 255 * 0.04045) {
                    rLinear = (rgbArray[1] * 1.0) / 12.92;
                } else {
                    rLinear = Math.pow(((rgbArray[1] * 1.0) + 0.055) / 1.055, 2.4);
                }
                if (rgbArray[2] <= 255 * 0.04045) {
                    gLinear = (rgbArray[2] * 1.0) / 12.92;
                } else {
                    gLinear = Math.pow(((rgbArray[2] * 1.0) + 0.055) / 1.055, 2.4);
                }
                if (rgbArray[3] <= 255 * 0.04045) {
                    bLinear = (rgbArray[3] * 1.0) / 12.92;
                } else {
                    bLinear = Math.pow(((rgbArray[3] * 1.0) + 0.055) / 1.055, 2.4);
                }

                // Merge the colors with some scalar
                double yLinear = (0.2126 * rLinear) + (0.7152 * gLinear) + (0.0722 * bLinear);

                // Add gamma back in relative to the new gray color
                double yRGB;
                if (yLinear <= 0.0031308) {
                    yRGB = 12.92 * yLinear;
                } else {
                    yRGB = 1.055 * Math.pow(yLinear, 1.0 / 2.4) - 0.055;
                }

                // Set each color to the same gray color
                rgbArray[1] = (int) Math.round(yRGB);
                rgbArray[2] = (int) Math.round(yRGB);
                rgbArray[3] = (int) Math.round(yRGB);

                picture[i][j] = getPixels(rgbArray);
            }
        }

        resetPicture();

    }

    private void EdgeDetection() {

        // Clean up the image
        Equalize();
        Grayscale();

        // Gaussian filter for blurring
        double gauss[][] = {
                {2.0 * (1.0 / 159.0), 4.0 * (1.0 / 159.0), 5.0 * (1.0 / 159.0), 4.0 * (1.0 / 159.0), 2.0 * (1.0 / 159.0)},
                {4.0 * (1.0 / 159.0), 9.0 * (1.0 / 159.0), 12.0 * (1.0 / 159.0), 9.0 * (1.0 / 159.0), 4.0 * (1.0 / 159.0)},
                {5.0 * (1.0 / 159.0), 12.0 * (1.0 / 159.0), 15.0 * (1.0 / 159.0), 12.0 * (1.0 / 159.0), 5.0 * (1.0 / 159.0)},
                {4.0 * (1.0 / 159.0), 9.0 * (1.0 / 159.0), 12.0 * (1.0 / 159.0), 9.0 * (1.0 / 159.0), 4.0 * (1.0 / 159.0)},
                {2.0 * (1.0 / 159.0), 4.0 * (1.0 / 159.0), 5.0 * (1.0 / 159.0), 4.0 * (1.0 / 159.0), 2.0 * (1.0 / 159.0)}
        };

        // 5x5 Mask for edge calculations
        double mask[][] = {
                {-1.0, -1.0, -1.0, -1.0, -1.0},
                {-1.0, 0.0, 0.0, 0.0, -1.0},
                {-1.0, 0.0, 16.0, 0.0, -1.0},
                {-1.0, 0.0, 0.0, 0.0, -1.0},
                {-1.0, -1.0, -1.0, -1.0, -1.0}
        };

        // Psuedo list comprehension to get just a single value out from the picture (gray)
        double working[][] = new double[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                working[i][j] = getPixelArray(picture[i][j])[2] * 1.0;
            }
        }

        // Apply gaussian filter
        double denoised[][] = new double[height][width];
        denoised = convolve(working, gauss);

        // Apply mask filter
        double edged[][] = new double[height][width];
        edged = convolve(denoised, mask);

        // Clamp the values outside of the range 0-255 to either 0 or 255 to better bring out the white/blacks
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int rgbArray[] = new int[4];

                rgbArray = getPixelArray(picture[i][j]);

                int newValue;
                if ((int) Math.round(edged[i][j]) <= 0) {
                    newValue = 0;
                } else if ((int) Math.round(edged[i][j]) >= 255) {
                    newValue = 255;
                } else {
                    newValue = (int) Math.round(edged[i][j]);
                }

                newValue = (newValue / 255) * 255;

                rgbArray[1] = newValue;
                rgbArray[2] = newValue;
                rgbArray[3] = newValue;

                picture[i][j] = getPixels(rgbArray);

            }
        }

        resetPicture();

    }

    // General convolution method
    private double[][] convolve(double[][] A, double[][] B) {
        double C[][] = new double[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                C[i][j] = 0;
                for (int k = -(B.length / 2); k < B.length - (B.length / 2); k++) {
                    for (int l = -(B.length / 2); l < B.length - (B.length / 2); l++) {
                        try {
                            C[i][j] += A[i + k][j + l] * B[k + (B.length / 2)][l + (B.length / 2)];
                        } catch (Exception e) {
                            C[i][j] += 0;
                        }
                    }
                }
            }
        }
        return C;
    }

    private void quit() {
        System.exit(0);
    }

    @Override
    public void mouseEntered(MouseEvent m) {
    }

    @Override
    public void mouseExited(MouseEvent m) {
    }

    @Override
    public void mouseClicked(MouseEvent m) {
        colorX = m.getX();
        colorY = m.getY();
        System.out.println(colorX + "  " + colorY);
        getValue();
        start.setEnabled(true);

        // Do these things when start is run
        start.addActionListener(evt -> {

            int targetPixel[] = getPixelArray(picture[colorY][colorX]);
            int range = 32; // Number of increments of color away from the target

            // Set every pixel to white if it is similar and black if it is not
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    int rgbArray[] = new int[4];
                    rgbArray = getPixelArray(picture[i][j]);

                    if (Math.abs(rgbArray[1] - targetPixel[1]) + Math.abs(rgbArray[2] - targetPixel[2]) + Math.abs(rgbArray[3] - targetPixel[3]) < range) {
                        rgbArray[1] = 255;
                        rgbArray[2] = 255;
                        rgbArray[3] = 255;
                    } else {
                        rgbArray[1] = 0;
                        rgbArray[2] = 0;
                        rgbArray[3] = 0;
                    }
                    picture[i][j] = getPixels(rgbArray);
                }
            }

            resetPicture();
        });
    }

    @Override
    public void mousePressed(MouseEvent m) {
    }

    @Override
    public void mouseReleased(MouseEvent m) {
    }

}

class MyPanel extends JPanel {

    private BufferedImage grid;
    private Color col;
    private double[] data;

    MyPanel(Color col) {
        this.col = col;
    }

    void loadData(double[] data) {
        this.data = data;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        if (grid == null) {
            int w = this.getWidth();
            int h = this.getHeight();
            grid = (BufferedImage) (this.createImage(w, h));
            Graphics2D gc = grid.createGraphics();

        }
        g2.drawImage(grid, null, 0, 0);
        g2.setColor(col);
        int step = 1;
        if (data != null) {
            for (int i = 0; i <= 255; i += step) {
                double newValue = 0;
                for (int j = 0; j <= step - 1; j++) {
                    newValue += data[i + j];
                }
                int ht = (int) Math.round((getHeight() - 16) * (newValue / step));
                g2.fillRect(i + 8, getHeight() - ht, step, ht);
            }
        }

    }

}